import { Container, Grid, TableContainer, Paper, Table, TableHead, TableRow, TableBody, TableCell, CircularProgress, Button } from "@mui/material";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { fetchUsers } from "../actions/users.actions";
import DetailModal from "./detailModal.component";

const Users = () => {
    const dispatch = useDispatch();

    const { users, pending } = useSelector((reduxData) => reduxData.userReducers);

    useEffect(() => {
        dispatch(fetchUsers());
    }, []);

    return (
        <Container>
            <Grid container mt={5}>
                { pending ? 
                    <Grid item md={12} sm={12} lg={12} xs={12} textAlign="center">
                        <CircularProgress />
                    </Grid>
                    :
                    <React.Fragment>
                        <Grid item md={12} sm={12} lg={12} xs={12}>
                            <TableContainer component={Paper}>
                                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Id</TableCell>
                                            <TableCell>Name</TableCell>
                                            <TableCell>country</TableCell>
                                            <TableCell>subject</TableCell>
                                            <TableCell>customerType</TableCell>
                                            <TableCell>registerStatus</TableCell>
                                            <TableCell>action</TableCell>
                                        </TableRow>
                                    </TableHead>        
                                    <TableBody>
                                        {users.map((element, index) => {
                                            return <>
                                                <TableRow key={element.id}>
                                                    <TableCell>{element.id}</TableCell>
                                                    <TableCell>{element.firstname} {element.lastname}</TableCell>
                                                    <TableCell>{element.country}</TableCell>
                                                    <TableCell>{element.subject}</TableCell>
                                                    <TableCell>{element.customerType}</TableCell>
                                                    <TableCell>{element.registerStatus}</TableCell>
                                                    <TableCell>
                                                        <DetailModal chosenUser={element}/>
                                                    </TableCell>
                                                </TableRow>
                                            </>
                                        })}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                    </React.Fragment>
                }
            </Grid>
        </Container>
    )
}

export default Users;