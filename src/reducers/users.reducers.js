import {
    FETCH_USERS_PENDING,
    FETCH_USERS_ERROR,
    FETCH_USERS_SUCCESS,
    CHOOSE_USER
} from "../constants/users.constants";

const initialState = {
    users: [],
    pending: false,
    chosenUser: {},
    modalOpen: false,
    error: null
}

export default function userReducers(state = initialState, action) {
    switch (action.type) {
        case FETCH_USERS_PENDING:
            state.pending = true;
            break;
        case FETCH_USERS_SUCCESS:
            state.pending = false;
            state.users = action.data;
            break;
        case FETCH_USERS_ERROR:
            break;
        case CHOOSE_USER:
            state.chosenUser = action.chosenUser;
            break;
        default:
            break;
    }

    return {...state};
}