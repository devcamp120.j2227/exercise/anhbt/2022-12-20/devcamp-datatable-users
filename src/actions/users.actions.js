import {
    FETCH_USERS_ERROR,
    FETCH_USERS_PENDING,
    FETCH_USERS_SUCCESS,
    CHOOSE_USER
} from "../constants/users.constants";

export const fetchUsers = () => {
    return async ( dispatch ) => {

        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        await dispatch({
            type: FETCH_USERS_PENDING
        });

        try {
            const response = await fetch("http://203.171.20.210:8080/devcamp-register-java-api/users", requestOptions);

            const data = await response.json();
            return dispatch({
                type: FETCH_USERS_SUCCESS,
                data: data.slice(0,50)
            })
        } catch (err) {
            return dispatch({
                type: FETCH_USERS_ERROR,
                error: err
            })
        }
    }
}

export const chooseUser = (user) => {
    return ({
        type: CHOOSE_USER,
        chosenUser: user,
    })
}